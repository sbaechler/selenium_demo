# coding: utf-8
from __future__ import unicode_literals, absolute_import
import time
from django.test import override_settings
from django_selenium.livetestcases import SeleniumLiveTestCase
from django.conf import settings
from selenium.common.exceptions import NoSuchWindowException
from selenium.webdriver.common.keys import Keys


class FacebookLoginTest(SeleniumLiveTestCase):
    fixtures = ['sites.json', 'socialaccount_socialapp.json']

    def tearDown(self):
        time.sleep(3)
        super(FacebookLoginTest, self).tearDown()


    def test_can_open_page(self):
        # store a reference to the main window.
        main_window_handle = self.driver.current_window_handle

        self.driver.get(self.live_server_url + '/')
        body = self.driver.find_element_by_tag_name('body')
        self.assertNotIn('Logout', body.text)
        self.assertIn('Sign Up', body.text)
        # Click on the Sign Up link.
        button = body.find_element_by_link_text('Sign Up')
        button.click()

        # Navigated to the Signup page.
        body = self.driver.find_element_by_tag_name('body')
        title = body.find_element_by_tag_name('h1')
        self.assertIn('Sign Up', title.text)
        fb_connect = body.find_element_by_link_text('Facebook Connect')
        fb_connect.click()

        # A popup window opens, asking for login:
        fb_window = self.driver.window_handles[1]
        self.driver.switch_to_window(fb_window)

        self.assertEqual('Facebook', self.driver.get_title())
        email = self.driver.find_element_by_id('email')
        email.send_keys(settings.FB_TESTUSER_EMAIL)
        email.send_keys(Keys.TAB)

        pw = self.driver.find_element_by_id('pass')
        pw.send_keys(settings.FB_TESTUSER_PW)
        pw.send_keys(Keys.ENTER)

        # The permission dialog appears (if the app is not installed.
        if len(self.driver.window_handles) == 2:
            self.assertEqual('Mit Facebook anmelden', self.driver.get_title())
            body = self.driver.find_element_by_tag_name('body')
            ok_button = body.find_element_by_name('__CONFIRM__')
            ok_button.click()

        # The user gets redirected to the main window.
        with self.assertRaises(NoSuchWindowException):
            self.assertTrue(self.driver.current_window_handle)
        self.driver.switch_to_window(main_window_handle)
        self.assertEqual('User: harry', self.driver.get_title())
        body = self.driver.find_element_by_tag_name('body')
        self.assertIn('Successfully signed in as harry', body.text)

