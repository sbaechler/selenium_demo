#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name="selenium_demo",
    version="0.1.0",
    author="Simon Bächler",
    author_email="sb@feinheit.ch",
    packages=[
        "selenium_demo",
    ],
    include_package_data=True,
    install_requires=[
        "Django==1.7.6",
    ],
    zip_safe=False,
    scripts=["selenium_demo/manage.py"],
)
